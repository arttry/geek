<?php

namespace app\models;

use yii\base\Model;


class Activity extends Model{
    
    public $title;
    
    public $description;
    
    public $date_start;
    
    public $is_blocked;
    
    public $author;

    public $date_finish;

    public $email;

    public $emailRepeat;

    public $use_notification;

    public $repeat_count;

    private $repeat_count_list = [0 => 'Не повторять', 1 => 'Один раз'];

    public function getRepeatCountList(){
        return $this->repeat_count_list;
    }

    public function beforeValidate() {
        if(!empty($this->date_start)){
            $date = \DateTime::createFromFormat('d.m.Y', $this->date_start);
            if($date){
                $this->date_start = $date->format('Y-m-d');
            }
        }
        return parent::beforeValidate();
    }

    public function rules() {
        return [
            ['title', 'required'],
            ['description', 'string', 'max'=>10, 'length' => 10],
            ['author', 'string'],
            ['title', 'trim'],
            ['date_start', 'date', 'format' => 'php:Y-m-d'],
            ['date_start', 'required'],
            ['date_finish', 'string'],
            ['email', 'email'],
            ['emailRepeat', 'compare', 'compareAttribute' => 'email'],
            ['email', 'required', 'when' => function($model){
                return $model->use_notification === 1;
            }],
            ['title', 'stopTitle'],
            [['title'], rules\StopTitleValidator::class],
            ['repeatCount', 'in', 'range' => array_keys($this->repeat_count_list)],
            ['is_blocked, use_notification', 'boolean']
        ];
    }
    
    public function stopTitle($attr) {
        if($this->$attr == 'admin'){
            $this->addError('title', 'Значения заголовка не допустимы');
        }
    }
    
    public function attributeLabels() {
        return [
            'title' => 'Заголовок',
            'description' => 'Описание',
            'date_start' => 'Дата начала',
            'is_blocked' => 'Заблокировано',
            'date_finish' => 'Дата конца',
            'author' => 'Автор',
            'repeat_count' => 'Количество повторов'
        ];
    }
    
}