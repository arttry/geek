<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StopTitleValidator
 *
 * @author Дмитрий
 */

namespace app\models\rules;

use yii\validators\Validator;


class StopTitleValidator extends Validator{
    
    public function validatorAttribute($model, $attribute) {
        
        if($this->$attr == 'admin'){
            $this->addError('title', 'Значения заголовка не допустимы');
        }
        
        
    }
    
    
    
}
